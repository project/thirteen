<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>


<div id="headwrapper">
        <div id="header">
		<?php if ($site_name) { ?><h1 id='title'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
        </div>
</div>

<div id="wrapper">
	<div id="content">
	<?php print $breadcrumb ?>
	<?php if($title): ?><h2 class="posttitle"><?php print $title ?></h2><?php endif; ?>
	<div class="tabs"><?php print $tabs ?></div>
	<?php print $content; ?>
	</div>
	
	<div id="sidebar">
	<ul>
	<li id="categories">
	<?php if ($sidebar_left): ?>
          <div id="sidebar-left">
          <?php print $sidebar_left ?>
          <?php if ($search_box): ?><li id="search"><?php print $search_box ?></li><?php endif; ?>
	  </div>
	<?php endif; ?>
	</li>
	</ul>
	</div>

	<div id="footer">
		<p>
		Original design by <a href="http://beccary.com">Beccary</a> ported to Drupal by vph
		</p>
	</div>

</div>
	
</body>
</html>
