
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php print t('By !username on !date', array('!username' => theme('username', $node), '!date' => format_date($node->created, 'custom', 'F dS, Y \a\t h:ia'))); ?>
    </span>
  <?php endif; ?>
  <?php if ($taxonomy): ?>
    <span class="terms">(<?php print $terms ?>)</span>
  <?php endif;?>

  <div class="content">
    <?php print $content ?>
  </div>

  <div class="clear-block clear">
    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>